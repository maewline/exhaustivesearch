/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.exhaustivesearch;

import java.util.ArrayList;

/**
 *
 * @author Arthi
 */
public class ExhaustiveSearchTest {
    public static void main(String[] args) {
        int[] a = {1,2,4,5};
        showInput(a);
        EXS exs = new EXS(a);
        exs.process(); 
        //show all pair subset
        exs.showAllSubset();
        exs.sum(); //sum and show result

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }
    }

